---
layout: single
title: About
permalink: /about/
author_profile: true
header: 
  teaser: /assets/images/
  header: /assets/images/
  og_image: /assets/images/
excerpt: ""
tags: [about me, thadigus, overview]
toc: true
---

As a systems administrator I am constantly trying to learn more about secure infrastructure and operations. After my degree in Cybersecurity I've aquired support skills and moved into systems administration. I have a passion for ethical hacking and I enjoy the process of creating scalable security automation solutions. While I was in college I founded the Trine University Hacking Team with [another student](https://www.linkedin.com/in/eric-gaby-mba-a64439194/). A lot of my time was spent on the board of our Computer Science Society, a group for CS students to come together and learn skills beyond the classroom. Along with my studies I was managing a server stack for professors to enhance student learning with hands on labs, teaching a study group for advanced senior-level hacking classes, and having a lot of fun.

With my first internship at [Weigand Construction](https://www.weigandconstruction.com/) I led the roll out and administration of an MDM to over 160 users and over 400 devices. I also covered level 1 helpdesk operations while I was on shift. As the school year rolled in, I continued my work at Weigand Construction with plans to hire on in the Spring. However, Parkview Health reached out to my professors and I was recommended for a Red Hat administration position to fill a newfound gap in on the healthcare team.

With my current position I am working on scalable security automations to ensure that, while the servers grow, the team does not. I provide solutions using Ansible and my expertise in Linux to maintain the structure and reliability of our patch management solution so that patching policies are followed even when humans are not involved. Products such as Red Hat Satellite and Ansible Automation Platform provide my team with the ability to scale their efforts and reduce human error. In this blog I hope to share the pieces of operations and infrastructure automation that I wish I would've known before diving in.

## Work/Educational History

### UNIX/EPIC Systems Administrator (Red Hat Administrator)

Parkview Health (Remote) Fort Wayne, IN --- December 2021 - Present

### IT Intern – Full-time Summer/Part-time School Year

Weigand Construction (Hybrid) Fort Wayne, IN --- March 2021 - December 2021

### Master of Business Administration

Trine University Angola, Indiana --- August 2020 - May 2023

### Bachelor of Science in Computer Science Information Technology with Cybersecurity Concentration

Trine University Angola, Indiana --- August 2019 - May 2022

## Skills

|---+---|
| Ethical Hacking/Penetration Testing | Information Systems Security |
| GitLab Administration and Use | SDLC/DevOps |
| MDM Administration | Networking Automation (Cisco and pfSense) |
| Virtualization (Proxmox, ESXi and vSphere) | WAF and Load Balancing (KEMP) |
| VPN Configuration (OpenVPN/Pritunl) | Change Management Processes |

## Certifications

[CompTIA Security+](https://www.credly.com/badges/7cc3513c-f909-4d95-8404-074a7b4bef1e?source=linked_in_profile)

[CompTIA CySA+](https://www.credly.com/badges/db2aa6b0-e829-4c75-8f5a-35114872e7f7/public_url)

[CompTIA Security Analytics Professional](https://www.credly.com/badges/3f0d4421-2a7f-40ab-9393-487306fb45c6/public_url)

## Extracurricular Activies in the IT Space

- ### [VMUG](https://www.vmug.com/)

  As an active member of the Fort Wayne VMware User Group I regularly attend talks by other memebers to learn about strategies in infrastructure and operations in a VMware environment.

- ### [BSides Fort Wayne](https://bsidesfortwayne.org/)

  I assisted in the first year of the Fort Wayne BSides chapter. In the second year I have served as the sponsorship coordinator, building relationships with businesses to secure funding and support for the organization.

- ### Trine University Computer Science Department Advisory Board

  Upon graduation and being named the Outstanding CSIT Student of 2022 I was invited to participate on the board of professionals that assist and advise the Trine University Computer Science Department. We work to ensure that the department is consistently up to date with its curriculum. While this is an investment in my own degree I also see it as an investment in future students and a way that I can foster the growth of IT and Cybersecurity students for the Greater Fort Wayne area.

## Hobbies

Outside of IT I work on my 1978 Ford Ranchero Squire and I enjoy getting away from my desk by fishing. It refreshes my mind to spend time outdoors, spending time with friends and family, as well as using my time to learn about other important aspects of life such as financial planning.

## More From Me

If you'd like to learn more about my work please visit my links on the side pane and feel free to contact me on LinkedIn and Discord!

[Contact Me - LinkedIn](https://www.linkedin.com/in/thadigus/)

[Contact Me - Discord](https://discordapp.com/users/201002397974134785)
