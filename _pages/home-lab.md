---
title: Home Lab Posts
layout: collection
permalink: /home-lab/
collection: home-lab
class: wide
author_profile: true
---

Documentation of my exploration and research as I learn to become a better cybersecurity practitioner.
Here I will show how I have configured my server rack and infrastructure. Typically these are Linux-related projects revolving around Infrastructure as Code implementations of modern technologies.
